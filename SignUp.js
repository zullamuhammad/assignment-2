import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, TextInput, TouchableOpacity, ScrollView } from 'react-native'
import Icon from 'react-native-vector-icons/Feather'
import Pkbm from '../assets/logopkbm.png'

class SignUp extends Component {
  constructor() {
    super()
    this.state = {
      hide: true
    }
  }

  buttonPress = () => {
    this.setState({
      fullName:'',
      username:'',
      email: '',
      password: '',
      cPassword:'',
    })
  }

  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <Image source={Pkbm} style={styles.logo} />
          <Text style={{ color: '#0174CF', marginTop: 20 }}>Please register with valid data</Text>

          <View style={styles.input}>
            <Icon name='user' size={20} style={{ color: '#0174CF', marginRight: 10 }} />
            <TextInput value={this.state.fullName} placeholder="Full Name" onChangeText={(e) => this.setState({ fullName: e })} />
          </View>

          <View style={styles.input}>
            <Icon name='user' size={20} style={{ color: '#0174CF', marginRight: 10 }} />
            <TextInput value={this.state.username} placeholder="Username" onChangeText={(e) => this.setState({ username: e })} />
          </View>

          <View style={styles.input}>
            <Icon name='mail' size={20} style={{ color: '#0174CF', marginRight: 10 }} />
            <TextInput value={this.state.email} placeholder="Email" onChangeText={(e) => this.setState({ email: e })} />
          </View>

          <View style={styles.input}>
            <Icon name='key' size={20} style={{ color: '#0174CF', marginRight: 10 }} />
            <TextInput value={this.state.password} secureTextEntry={this.state.hide} placeholder='Password' onChangeText={(e) => this.setState({ password: e })} style={{ flex: 1 }} />
            <TouchableOpacity onPress={() => this.setState({ hide: !this.state.hide })}>
              <Icon name={this.state.hide ? 'eye-off' : 'eye'} size={20} style={{ color: '#0174CF' }} />
            </TouchableOpacity>
          </View>

          <View style={styles.input}>
            <Icon name='key' size={20} style={{ color: '#0174CF', marginRight: 10 }} />
            <TextInput value={this.state.cPassword} secureTextEntry={this.state.hide} placeholder='Confirm Password' onChangeText={(e) => this.setState({cPassword: e })} style={{ flex: 1 }} />
            <TouchableOpacity onPress={() => this.setState({ hide: !this.state.hide })}>
              <Icon name={this.state.hide ? 'eye-off' : 'eye'} size={20} style={{ color: '#0174CF' }} />
            </TouchableOpacity>
          </View>

          <TouchableOpacity style={styles.button} onPress={this.buttonPress}>
            <Text style={{ color: '#fff' }}>Register</Text>
          </TouchableOpacity>

          <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20 }}>
            <Text> Already have an account ? </Text>
            <TouchableOpacity>
              <Text style={{ color: '#0174CF' }} onPress={() => this.props.navigation.navigate('Login')}> Login </Text>
            </TouchableOpacity>
          </View>

          <Text style={{ color: '#0174CF', fontSize: 10, marginTop: 20 }}> Design by zullamuhammad</Text>

        </View>
      </ScrollView>

    )
  }
}

export default SignUp

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 20
  },
  logo: {
    width: 150,
    height: 150,
  },
  input: {
    width: 270,
    paddingHorizontal: 15,
    borderWidth: 0.3,
    borderRadius: 50,
    marginTop: 20,
    marginHorizontal: 10,
    elevation: 5,
    backgroundColor: 'white',
    borderColor: '#0174CF',
    flexDirection: 'row',
    alignItems: 'center',
  },
  button: {
    width: 180,
    paddingVertical: 16,
    marginTop: 20,
    borderRadius: 50,
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#0174CF',
    elevation: 5,
    backgroundColor: '#0174CF'
  }
}

)