import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, TextInput, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/Feather'
import Logo from '../assets/logopkbm.png'

class LoginForm extends Component {
  constructor() {
    super()
    this.state = {
      hide: true
    }
  }

  buttonPress = () => {
    this.setState({
      email: '',
      password: '',
    })
  }


  render() {
    return (
      <View style={styles.container}>
        <Image source={Logo} style={styles.logo} />
        <Text style={{ color: '#0174CF', marginTop: 20 }}>Please login with a resgitered acccount</Text>

        <View style={styles.input}>
          <Icon name='user' size={20} style={{ color: '#0174CF', marginRight: 10 }} />
          <TextInput value={this.state.email} placeholder="Email / Username" onChangeText={(e) => this.setState({ email: e })} />
        </View>

        <View style={styles.input}>
          <Icon name='key' size={20} style={{ color: '#0174CF', marginRight: 10 }} />
          <TextInput value={this.state.password} secureTextEntry={this.state.hide} placeholder='Password' onChangeText={(e) => this.setState({ password: e })} style={{ flex: 1 }} />
          <TouchableOpacity onPress={() => this.setState({ hide: !this.state.hide })}>
            <Icon name={this.state.hide ? 'eye-off' : 'eye'} size={20} style={{ color: '#0174CF' }} />
          </TouchableOpacity>
        </View>

        <TouchableOpacity style={styles.button} onPress={this.buttonPress}>
          <Text style={{ color: '#fff' }}>LOGIN</Text>
        </TouchableOpacity>

        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20 }}>
          <Text> Forgot Password ? </Text>
          <TouchableOpacity>
            <Text style={{ color: '#0174CF' }} onPress={() => this.props.navigation.navigate('Reset Password')}> Reset Password </Text>
          </TouchableOpacity>
        </View>

        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
          <Text> Don't have an account ? </Text>
          <TouchableOpacity>
            <Text style={{ color: '#0174CF' }} onPress={() => this.props.navigation.navigate('SignUp')}> Sign Up </Text>
          </TouchableOpacity>
        </View>

        <Text style={{ color: '#0174CF', fontSize: 10, marginTop: 100 }}> Design by zullamuhammad</Text>
      </View>
    )
  }
}

export default LoginForm

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white'
  },
  logo: {
    width: 200,
    height: 200
  },
  input: {
    width: 270,
    paddingHorizontal: 15,
    borderWidth: 0.3,
    borderRadius: 50,
    marginTop: 20,
    marginHorizontal: 10,
    elevation: 5,
    backgroundColor: 'white',
    borderColor: '#0174CF',
    flexDirection: 'row',
    alignItems: 'center',
  },
  button: {
    width: 180,
    paddingVertical: 16,
    marginTop: 20,
    borderRadius: 50,
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#0174CF',
    elevation: 5,
    backgroundColor: '#0174CF'
  }
}

)