import React, { Component } from 'react';
import {View, ImageBackground, Image, Text } from 'react-native';
import Logo from '../assets/logopkbm.png'

export default class SplashScreen extends Component {
    constructor() {
        super();
        this.state = {};
    }

    componentDidMount() {
        setTimeout(() => {
            this.props.navigation.replace('Login');
        }, 2000);
    }

    render() {
        return (

            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor:'#fff' }}>
                <Image source={Logo} style={{width: 150, height: 150}} />
            </View>

        )
    }
}
